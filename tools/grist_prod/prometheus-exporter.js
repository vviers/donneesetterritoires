const { collectDefaultMetrics, register } = require('prom-client');
const http = require('http');

if (process.argv.some(arg => arg.includes("_build/stubs/app/server/server.js"))) {
  console.log("--------------------");
  console.log("[ANCT] Starting prometheus-exporter.js");

  collectDefaultMetrics({ timeout: 10_000 });

  const server = http.createServer((req, res) => {
    register.metrics().then((metrics) => {
      res.writeHead(200, { 'Content-Type': register.contentType });
      res.end(metrics);
    }).catch((e) => {
      res.writeHead(500);
      res.end(e.message);
    });
  })

  server.listen(parseInt(process.env.GRIST_ANCT_PROMCLIENT_PORT || 9091), '0.0.0.0');
  console.log("[ANCT] Started prometheus-exporter.js");
  console.log("--------------------");
}
