const minio = require('minio');

const orig = minio.Client.prototype.removeObjects;

async function doRemoveObjects(bucket, objects) {
  for (const obj of objects) {
    try {
      await this.removeObject(bucket, obj.name, { versionId: obj.versionId });
    } catch(e) {
      console.error('Error occurred while removing object %s with versionId=%s, continuing anyway.\n Error = %s', obj.name, obj.versionId, e.stack);
    }
  }
}

minio.Client.prototype.removeObjects = function(...args) {
  if (Array.isArray(args[1]) && args.length === 2) {
    const objects = args[1];
    if (objects.length && objects[0].versionId) {
      return doRemoveObjects.call(this, args[0], objects);
    }
  }
  return orig.apply(this, args);
}

