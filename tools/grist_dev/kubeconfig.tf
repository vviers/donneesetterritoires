module "kubeconfig_florent_fayolle" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "0.3.1"

  filename               = "grist-florent-fayolle-dev.yml"
  namespace              = module.grist_namespace.namespace
  username               = "florent-fayolle"
  project_name           = "umap"
  role_name              = module.grist_namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}
