# Notes for migrating a Grist instance to use snapshots with S3

## Timeline

### A week before

Update the instance to override the main html document to show a popup to users to warn them about an upcoming maintenance.

### Any time before

Update the instance to use an image version >= 1.1.3.

### When starting the operation

Deploy :
- the maintenance page using the same domain as Grist
- traefik forward auth with the ingress disabled
- the redis instance
- the snapshots bucket with versionning enabled
- Grist, with:
  - configuration to use the `sleep infinity` command instead of its entrypoint
  - the environment variables for the bucket and redis instance

### Triggering a backup

The backup job should be triggered :
```
kubectl delete job manual
kubectl create job --from cronjob/grist-backup manual
```

### Running the migration script

When the backup is done, copy the migration script archive to the Grist pod, extract it in a temporary folder, and run the script with the path to the `docs/` folder as argument.

### When the migration is done

Deploy:
- traefik forward auth with its ingress back
- grist without the `sleep infinity` command
- remove the maintenance page
