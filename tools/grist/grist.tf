locals {
  grist_release_name = "grist"
}

resource "scaleway_object_bucket" "grist_backups" {
  name   = var.backup_bucket
  region = "fr-par"
}

resource "scaleway_object_bucket" "grist_snapshots" {
  name = "${var.project_slug}-snapshots"
  versioning {
    enabled = true
  }
}

resource "random_password" "redis_password" {
  length  = 32
  special = false
}

module "redis" {
  source        = "gitlab.com/vigigloo/tools-k8s/redis"
  version       = "0.1.1"
  namespace     = module.grist_namespace.namespace
  chart_name    = "redis"
  chart_version = "17.4.2"

  redis_password = resource.random_password.redis_password.result
  redis_replicas = 0
}

module "grist_namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "2.0.2"
  max_cpu_requests  = "4"
  max_memory_limits = "12Gi"
  namespace         = var.project_slug
  project_name      = "Grist"
  project_slug      = var.project_slug

  default_container_cpu_requests  = "200m"
  default_container_memory_limits = "128Mi"
}

module "grist" {
  source        = "gitlab.com/vigigloo/tools-k8s/grist"
  version       = "1.1.1"
  namespace     = module.grist_namespace.namespace
  chart_name    = local.grist_release_name
  chart_version = "1.3.0"
  values = [
    <<-EOT
    podAnnotations:
      monitoring-org-id: ${var.monitoring_org_id}
      prometheus.io/scrape: 'true'
      prometheus.io/path: '/'
      prometheus.io/port: '9091'
    envVars:
      APP_HOME_URL: https://${var.domain}
      GRIST_FORWARD_AUTH_HEADER: X-Forwarded-User
      GRIST_FORWARD_AUTH_LOGOUT_PATH: _oauth/logout
      GRIST_ORG_IN_PATH: "true"
      GRIST_DEFAULT_EMAIL: "${var.default_email}"
      GRIST_SANDBOX_FLAVOR: gvisor
      GRIST_HIDE_UI_ELEMENTS: billing
      APP_STATIC_INCLUDE_CUSTOM_CSS: true
      GRIST_DEFAULT_LOCALE: fr
      GRIST_HELP_CENTER: https://outline.incubateur.anct.gouv.fr/doc/documentation-grist-YPWlYTHa8j
      GRIST_DOCS_MINIO_BUCKET: ${scaleway_object_bucket.grist_snapshots.name}
      GRIST_DOCS_MINIO_ENDPOINT: s3.fr-par.scw.cloud
      GRIST_DOCS_MINIO_BUCKET_REGION: fr-par
      GRIST_DOCS_MINIO_ACCESS_KEY: ${var.scaleway_project_config.access_key}
      GRIST_DOCS_MINIO_SECRET_KEY: ${var.scaleway_project_config.secret_key}
      REDIS_URL: redis://default:${resource.random_password.redis_password.result}@${module.redis.hostname}
      GRIST_ANCT_PROMCLIENT_PORT: "9091"
      GRIST_ANON_PLAYGROUND: false
    backup:
      image:
        tag: ab3372d35cb21a8c6d86c107d77e69f0c3138bb9
    command: ["/bin/sh","-c"]
    args: ["cd /grist && yarn add prom-client && export NODE_OPTIONS='--max-old-space-size=4096 -r /grist/prometheus-exporter.js -r /grist/patch-scaleway.js' && ./sandbox/run.sh"]
    probes:
      liveness: null
      readiness: null
    EOT
  ]
  limits_memory = "8Gi"
  requests_cpu  = "200m"

  backup_limits_memory = "1Gi"
  backup_requests_cpu  = "1"

  grist_persistence_size     = "20Gi"
  grist_backup_schedule      = "0 3 * * *"
  grist_backup_s3_endpoint   = scaleway_object_bucket.grist_backups.endpoint
  grist_backup_s3_region     = scaleway_object_bucket.grist_backups.region
  grist_backup_s3_bucket     = scaleway_object_bucket.grist_backups.name
  grist_backup_s3_access_key = var.scaleway_access_key
  grist_backup_s3_secret_key = var.scaleway_secret_key

  image_repository = "gristlabs/grist"
  image_tag        = "1.1.4"

  grist_mount_files = [
    {
      path    = "/grist/static/ui-icons/Logo/logo_anct_2022.svg"
      content = filebase64("${path.module}/logo_anct_2022.svg")
    },
    {
      path    = "/grist/static/custom.css"
      content = filebase64("${path.module}/custom.css")
    },
    {
      path    = "/grist/prometheus-exporter.js"
      content = filebase64("${path.module}/prometheus-exporter.js")
    },
    {
      path    = "/grist/patch-scaleway.js"
      content = filebase64("${path.module}/patch-scaleway.js")
    }
  ]
}

resource "random_password" "auth_secret" {
  length  = 64
  special = false
}

module "auth" {
  source     = "gitlab.com/vigigloo/tools-k8s/traefikforwardauth"
  version    = "0.1.1"
  namespace  = module.grist_namespace.namespace
  chart_name = "auth"
  values = [
    <<-EOT
    ingress:
      annotations:
        haproxy.org/cors-enable: "true"
        haproxy.org/cors-allow-origin: "^https://.*\\.anct\\.gouv\\.fr$"
        haproxy.org/cors-allow-methods: "GET"
        haproxy.org/cors-allow-headers: "*"
    middleware:
      envVars:
        INSECURE_COOKIE: "true"
        DEFAULT_PROVIDER: "generic-oauth"
        PROVIDERS_GENERIC_OAUTH_AUTH_URL: "https://${var.oauth_domain}/oauth/authorize"
        PROVIDERS_GENERIC_OAUTH_TOKEN_URL: "https://${var.oauth_domain}/oauth/token"
        PROVIDERS_GENERIC_OAUTH_USER_URL: "https://${var.oauth_domain}/oauth/userinfo"
        PROVIDERS_GENERIC_OAUTH_CLIENT_ID: "${var.oauth_client_id}"
        PROVIDERS_GENERIC_OAUTH_CLIENT_SECRET: "${var.oauth_client_secret}"
        PROVIDERS_GENERIC_OAUTH_SCOPE: "openid email organizations"
        SECRET: ${random_password.auth_secret.result}
        LOGOUT_REDIRECT: "https://${var.domain}/signed-out"
EOT
  ]
  limits_cpu      = 1
  limits_memory   = "1Gi"
  requests_cpu    = "10m"
  requests_memory = "40Mi"
  ingress_host    = var.domain
  auth_paths = [
    "/auth/login",
    "/_oauth"
  ]
  auth_target_service = "http://${local.grist_release_name}"
}
