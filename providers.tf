provider "scaleway" {
  alias      = "development"
  region     = "fr-par"
  zone       = "fr-par-1"
  access_key = var.scaleway_cluster_development_access_key
  secret_key = var.scaleway_cluster_development_secret_key
  project_id = var.scaleway_cluster_development_project_id
}

provider "scaleway" {
  alias      = "production"
  region     = "fr-par"
  zone       = "fr-par-1"
  access_key = var.scaleway_cluster_production_access_key
  secret_key = var.scaleway_cluster_production_secret_key
  project_id = var.scaleway_cluster_production_project_id
}

provider "scaleway" {
  alias      = "project"
  region     = "fr-par"
  zone       = "fr-par-1"
  access_key = var.scaleway_project_config.access_key
  secret_key = var.scaleway_project_config.secret_key
  project_id = var.scaleway_project_config.project_id
}

locals {
  kubeconfig_production  = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  kubeconfig_development = data.scaleway_k8s_cluster.dev.kubeconfig[0]
}
provider "kubernetes" {
  alias = "production"
  host  = local.kubeconfig_production.host
  token = local.kubeconfig_production.token

  cluster_ca_certificate = base64decode(local.kubeconfig_production.cluster_ca_certificate)
}
provider "kubernetes" {
  alias = "development"
  host  = local.kubeconfig_development.host
  token = local.kubeconfig_development.token

  cluster_ca_certificate = base64decode(local.kubeconfig_development.cluster_ca_certificate)
}
provider "helm" {
  alias = "production"
  kubernetes {
    host  = local.kubeconfig_production.host
    token = local.kubeconfig_production.token

    cluster_ca_certificate = base64decode(local.kubeconfig_production.cluster_ca_certificate)
  }
}
provider "helm" {
  alias = "development"
  kubernetes {
    host  = local.kubeconfig_development.host
    token = local.kubeconfig_development.token

    cluster_ca_certificate = base64decode(local.kubeconfig_development.cluster_ca_certificate)
  }
}

provider "gitlab" {
  token = var.gitlab_token
}

provider "grafana" {
  alias  = "production"
  url    = var.grafana_production_config.url
  auth   = var.grafana_production_config.api_key
  org_id = var.grafana_production_config.org_id
}

provider "grafana" {
  alias  = "development"
  url    = var.grafana_development_config.url
  auth   = var.grafana_development_config.api_key
  org_id = var.grafana_development_config.org_id
}
