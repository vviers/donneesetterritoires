data "scaleway_object_bucket" "runner_bucket" {
  provider = scaleway.development
  name     = "devops-gitlab-runner-cache"
  region   = "fr-par"
}

module "gitlab_runner" {
  source  = "gitlab.com/vigigloo/tf-modules/k8sgitlabrunner"
  version = "0.2.6"

  # @vigigloo source "helm+https://charts.gitlab.io#name=gitlab-runner" "^0.56"
  chart_version = "0.56.0"
  kubeconfig    = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  project_slug  = var.project_slug
  project_name  = var.project_name
  gitlab_groups = [data.gitlab_group.donneesetterritoires.group_id]

  cache_provider         = "s3"
  cache_s3_host          = "s3.fr-par.scw.cloud"
  cache_s3_bucket-name   = data.scaleway_object_bucket.runner_bucket.name
  cache_s3_bucket-region = data.scaleway_object_bucket.runner_bucket.region
  cache_s3_access-key    = var.scaleway_cluster_development_access_key
  cache_s3_secret-key    = var.scaleway_cluster_development_secret_key
}

module "addok" {
  source = "./tools/addok"

  kubeconfig   = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  hostname     = "addok.${var.dev_base_domain}"
  project_slug = var.old_project_slug
}

locals {
  grist_beta_project_slug = "${var.project_slug}-grist-beta"
}
module "grist_beta" {
  source                  = "./tools/grist"
  default_email           = var.tools_grist_beta_default_email
  monitoring_org_id       = random_string.production_secret_org_id.result
  oauth_client_id         = var.tools_grist_beta_oauth_client_id
  oauth_client_secret     = var.tools_grist_beta_oauth_client_secret
  oauth_domain            = var.tools_grist_beta_oauth_domain
  domain                  = "grist.incubateur.net"
  project_slug            = local.grist_beta_project_slug
  scaleway_project_config = var.scaleway_project_config
  scaleway_access_key     = var.scaleway_project_config.access_key
  scaleway_secret_key     = var.scaleway_project_config.secret_key
  backup_bucket           = "grist-beta-backups"

  # explicitly passing providers to avoid confusion
  providers = {
    helm       = helm.production
    kubernetes = kubernetes.production
    scaleway   = scaleway.project
  }
}

module "grist_dev" {
  source                  = "./tools/grist_dev"
  scaleway_project_config = var.scaleway_project_config
  scaleway_access_key     = var.scaleway_cluster_development_access_key
  scaleway_secret_key     = var.scaleway_cluster_development_secret_key
  dns_zone_incubateur     = "incubateur.anct.gouv.fr"
  oauth_domain            = var.development_tools_grist_oauth_domain
  oauth_client_id         = var.development_tools_grist_oauth_client_id
  oauth_client_secret     = var.development_tools_grist_oauth_client_secret
  default_email           = var.development_tools_grist_default_email
  kubeconfig              = local.kubeconfig_development
  project_slug            = "${var.project_slug}-grist-development"

  monitoring_org_id = var.grist_monitoring_org_id
  providers = {
    kubernetes       = kubernetes.development
    helm             = helm.development
    scaleway.old     = scaleway.development
    scaleway.project = scaleway.project
  }
}

module "grist_prod" {
  source                  = "./tools/grist_prod"
  scaleway_project_config = var.scaleway_project_config
  dns_zone_incubateur     = "incubateur.anct.gouv.fr"
  oauth_client_id         = var.production_tools_grist_oauth_client_id
  oauth_client_secret     = var.production_tools_grist_oauth_client_secret
  oauth_domain            = var.production_tools_grist_oauth_domain
  default_email           = var.production_tools_grist_default_email
  project_slug            = "${var.project_slug}-grist-production"

  monitoring_org_id = var.grist_monitoring_org_id
  providers = {
    helm             = helm.production
    kubernetes       = kubernetes.production
    scaleway.old     = scaleway.production
    scaleway.project = scaleway.project
  }
}
